package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Kviz implements Parcelable {
    public String naziv;
    public ArrayList<Pitanje> pitanja= new ArrayList<>();
    public Kategorija kategorija;

    public Kviz(String n,ArrayList<Pitanje>p,Kategorija k){
        this.naziv=n;
        this.pitanja=p;
        this.kategorija=k;
    }
    protected Kviz(Parcel in) {
        naziv = in.readString();
        pitanja=in.createTypedArrayList(Pitanje.CREATOR);
        kategorija=in.readParcelable(getClass().getClassLoader());
    }

    public static final Creator<Kviz> CREATOR = new Creator<Kviz>() {
        @Override
        public Kviz createFromParcel(Parcel in) {
            return new Kviz(in);
        }

        @Override
        public Kviz[] newArray(int size) {
            return new Kviz[size];
        }
    };

    public Kviz() {

    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Pitanje> getPitanja() {
        return pitanja;
    }

    public void setPitanja(ArrayList<Pitanje> pitanja) {
        this.pitanja = pitanja;
    }

    public Kategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(Kategorija kategorija) {
        this.kategorija = kategorija;
    }
    public void dodajPitanje(Pitanje novo){
        pitanja.add(novo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeTypedList(pitanja);
        dest.writeParcelable(kategorija,0 );
    }
}
