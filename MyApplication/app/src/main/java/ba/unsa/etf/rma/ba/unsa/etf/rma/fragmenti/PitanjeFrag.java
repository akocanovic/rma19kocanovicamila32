package ba.unsa.etf.rma.ba.unsa.etf.rma.fragmenti;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import ba.unsa.etf.rma.aktivnosti.DodajPitanjeAkt;
import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.klase.Adapter;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class PitanjeFrag extends Fragment {
    private fragmentPitanje listener;


    public interface fragmentPitanje {
        void inputPitanje(Pitanje p,int vel);
    }
    public PitanjeFrag() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pitanje, container, false);
    }
    ArrayList<String> odgovoriSvi=new ArrayList<>();
    String tacni=new String(" ");
    Pitanje item=new Pitanje();
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final TextView tekstPitanja=(TextView) getView().findViewById(R.id.tekstPitanja);
        final ListView odgovoriPitanja=(ListView) getView().findViewById(R.id.odgovoriPitanja);

        final String kviz= getArguments().getString("odabraniKviz");
        final ArrayList<Kviz> primljeniKvizovi= getArguments().getParcelableArrayList("kviz");
        Kviz nasKviz=new Kviz();
        for(int i=0;i<primljeniKvizovi.size();i++){
            if(primljeniKvizovi.get(i).getNaziv().equals(kviz)){
                nasKviz=primljeniKvizovi.get(i);
            }
        }

        Random rand = new Random();

        item = nasKviz.getPitanja().get(rand.nextInt(nasKviz.getPitanja().size()));
        final String tacanOdg=item.getTacan();
        tekstPitanja.setText(item.getTekstPitanja());
        odgovoriSvi=item.getOdgovori();
        final ArrayAdapter adapterOdgovora=new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,odgovoriSvi);
        odgovoriPitanja.setAdapter(adapterOdgovora);
        adapterOdgovora.notifyDataSetChanged();
        final Kviz finalNasKviz = nasKviz;
        odgovoriPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                if (finalNasKviz.getPitanja().size() != 0){
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void run() {
                            String odabraniOdg = (String) odgovoriPitanja.getItemAtPosition(position);
                if (odabraniOdg.equals(tacanOdg) || odabraniOdg.equals(tacni)) {
                    item.setTacan("xx");
                    parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.zelena));
                    adapterOdgovora.notifyDataSetChanged();
                    listener.inputPitanje(item, finalNasKviz.getPitanja().size());
                } else {
                    parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.crvena));
                    adapterOdgovora.notifyDataSetChanged();
                    for(int i =0;i<odgovoriPitanja.getChildCount();i++){
                        if(odgovoriPitanja.getChildAt(i).equals(tacni)){
                            parent.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.zelena));
                            adapterOdgovora.notifyDataSetChanged();
                        }
                    }
                    listener.inputPitanje(item,finalNasKviz.getPitanja().size());
                }
                for (int i = 0; i < finalNasKviz.getPitanja().size(); i++) {
                    if (finalNasKviz.getPitanja().get(i).getTekstPitanja().equals(item.getTekstPitanja())) {
                        finalNasKviz.getPitanja().remove(i);
                    }
                }
                adapterOdgovora.notifyDataSetChanged();
                if (finalNasKviz.getPitanja().size() == 0) {
                                tekstPitanja.setText("Nema vise pitanja!");
                                odgovoriSvi.clear();
                                final ArrayAdapter adapterOdgovora1 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, odgovoriSvi);
                                odgovoriPitanja.setAdapter(adapterOdgovora1);
                                adapterOdgovora1.notifyDataSetChanged();
                }
                if (finalNasKviz.getPitanja().size() != 0) {
                    Random rand = new Random();
                    item = finalNasKviz.getPitanja().get(rand.nextInt(finalNasKviz.getPitanja().size()));
                    tacni = item.getTacan();
                    tekstPitanja.setText(item.getTekstPitanja());
                    odgovoriSvi = item.getOdgovori();
                    final ArrayAdapter adapterOdgovora = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, odgovoriSvi);
                    odgovoriPitanja.setAdapter(adapterOdgovora);
                    adapterOdgovora.notifyDataSetChanged();
                }
                    }
                }, 2000);
            }
            else{
                    tekstPitanja.setText("Nema vise pitanja!");
                    adapterOdgovora.notifyDataSetChanged();
                }
            }

        });


    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof fragmentPitanje) {
            listener = (fragmentPitanje) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


}
