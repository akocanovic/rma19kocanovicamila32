package ba.unsa.etf.rma.aktivnosti;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodajPitanjeAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_pitanje_akt);

        final ListView lvOdgovori=(ListView) findViewById(R.id.lvOdgovori);
        final EditText etOdgovor= (EditText) findViewById(R.id.etOdgovor);
        final EditText etNaziv=(EditText) findViewById(R.id.etNaziv);
        final TextView odgovori=(TextView) findViewById(R.id.odgovori);
        final Button btnDodajOdgovor=(Button) findViewById(R.id.btnDodajOdgovor);
        final Button btnDodajTacan=(Button) findViewById(R.id.btnDodajTacan);
        final Button btnDodajPitanje =(Button) findViewById(R.id.btnDodajPitanje);

        final ArrayList<String> sviOdgovori=new ArrayList<>();
        Bundle intent= getIntent().getExtras();
        final ArrayList<Kviz> svi= intent.getParcelableArrayList("xx");
        final ArrayAdapter<String> adapterOdgovora=new ArrayAdapter<>(DodajPitanjeAkt.this,android.R.layout.simple_list_item_1,sviOdgovori);
        lvOdgovori.setAdapter(adapterOdgovora);
        lvOdgovori.setOnItemClickListener(new AdapterView.OnItemClickListener() {                   // odgovori
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sviOdgovori.remove(sviOdgovori.get(position));
                adapterOdgovora.notifyDataSetChanged();
            }
        });
        btnDodajOdgovor.setOnClickListener(new View.OnClickListener() {                             // dodavanje obicnog odgovora
            @Override
            public void onClick(View v) {
                if(etOdgovor.getText().toString().isEmpty() || sviOdgovori.contains(etOdgovor.getText().toString())){
                    etOdgovor.setBackgroundColor(Color.RED);
                }else {
                    sviOdgovori.add(etOdgovor.getText().toString());
                    adapterOdgovora.notifyDataSetChanged();
                }
            }
        });

        btnDodajTacan.setOnClickListener(new View.OnClickListener() {                               //dodavanje tacnog odgovora
            @Override
            public void onClick(View v) {
                etOdgovor.setBackgroundColor(Color.WHITE);
                if(etOdgovor.getText().toString().isEmpty() || sviOdgovori.contains(etOdgovor.getText().toString())){
                    etOdgovor.setBackgroundColor(Color.RED);
                }else {
                    String tacan;
                    int vel = (sviOdgovori.size()) + 1;
                    sviOdgovori.add(etOdgovor.getText().toString());
                    adapterOdgovora.notifyDataSetChanged();
                    btnDodajTacan.setEnabled(false);
                    //lvOdgovori.getChildAt(vel).setBackgroundColor(Color.GREEN);
                }
            }

        });

        btnDodajPitanje.setOnClickListener(new View.OnClickListener() {                             //dodaj citavo pitanje
            @Override
            public void onClick(View v) {
                for(int i =0;i<svi.size();i++) {
                    if (svi.get(i).getNaziv().equals(etNaziv.getText().toString())) {
                        etNaziv.setBackgroundColor(Color.RED);
                    }
                }
                if(etNaziv.getText().toString().isEmpty()){
                    etNaziv.setBackgroundColor(Color.RED);

                }else{
                    Pitanje novo;
                    novo = new Pitanje(etNaziv.getText().toString(), etNaziv.getText().toString(), sviOdgovori, etOdgovor.getText().toString());
                    Intent intent2 = new Intent(DodajPitanjeAkt.this, DodajKvizAkt.class);
                    intent2.putExtra("noviKvizovi", novo);
                    setResult(2, intent2);
                    finish();
                }
            }
        });
    }
}
