package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.amila.myapplication.R;

import static ba.unsa.etf.rma.amila.myapplication.R.mipmap.ic_launcher;

public class Adapter extends ArrayAdapter<Kviz> {
    public Adapter(@NonNull Context context, ArrayList<Kviz> kvizovi) {
        super(context,R.layout.element_liste,kvizovi );
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Kviz kviz=getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_liste, parent, false);
        }
     /*   View vi = convertView;
        LayoutInflater v = LayoutInflater.from(getContext());
        vi= v.inflate( R.layout.element_liste, parent, false);*/
        final TextView naziv= (TextView) convertView.findViewById(R.id.naziv);
        final ImageView slikica= (ImageView) convertView.findViewById(R.id.slikica);

        naziv.setText(kviz.naziv);
        slikica.setImageResource(R.drawable.ic_launcher_background);
        return convertView;
    }
}
