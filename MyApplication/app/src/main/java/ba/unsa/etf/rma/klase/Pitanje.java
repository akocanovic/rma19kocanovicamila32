package ba.unsa.etf.rma.klase;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;


public class Pitanje implements Parcelable {
    public String naziv;
    public String tekstPitanja;
    public ArrayList<String>  odgovori=new ArrayList<>();
    public String tacan;

    public Pitanje(String n,String tekst, ArrayList<String> odg,String t){
        this.naziv=n;
        this.tekstPitanja=tekst;
        this.odgovori=odg;
        this.tacan=t;
    }

    protected Pitanje(Parcel in) {
        naziv = in.readString();
        tekstPitanja = in.readString();
        odgovori = in.createStringArrayList();
        tacan = in.readString();
    }

    public static final Creator<Pitanje> CREATOR = new Creator<Pitanje>() {
        @Override
        public Pitanje createFromParcel(Parcel in) {
            return new Pitanje(in);
        }

        @Override
        public Pitanje[] newArray(int size) {
            return new Pitanje[size];
        }
    };
    public Pitanje(){

    }
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getTekstPitanja() {
        return tekstPitanja;
    }

    public void setTekstPitanja(String tekstPitanja) {
        this.tekstPitanja = tekstPitanja;
    }

    public ArrayList<String> getOdgovori() {
        return odgovori;
    }

    public void setOdgovori(ArrayList<String> odgovori) {
        this.odgovori = odgovori;
    }

    public String getTacan() {
        return tacan;
    }

    public void setTacan(String tacan) {
        this.tacan = tacan;
    }
    public  ArrayList<String> dajRandomOdgovore(){
        ArrayList<String> odg=new ArrayList<>();

        return odg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeString(tekstPitanja);
        dest.writeStringList(odgovori);
        dest.writeString(tacan);
    }
}
