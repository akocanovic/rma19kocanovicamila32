package ba.unsa.etf.rma.aktivnosti;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.ba.unsa.etf.rma.fragmenti.PitanjeFrag;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class IgrajKvizAkt extends AppCompatActivity implements PitanjeFrag.fragmentPitanje, InformacijeFrag.fragmentInformacije{
    InformacijeFrag info= new InformacijeFrag();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igraj_kviz_akt);

        Bundle intent= getIntent().getExtras();
        Intent in1=getIntent();
        final String odabraniKviz=in1.getStringExtra("odabrani");
        final ArrayList<Kviz> primljeniKvizovi= intent.getParcelableArrayList("kvizovi");
        final ArrayList<Kategorija> primljenjeKategorije = intent.getParcelableArrayList("kat");

        android.support.v4.app.FragmentManager fragmentManager= getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft= fragmentManager.beginTransaction();
        //InformacijeFrag info=new InformacijeFrag();
        //ft.replace(R.id.fragment_container,info);
        final Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("kviz", primljeniKvizovi);
        bundle.putString("odabraniKviz",odabraniKviz );
        info.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.fragment_container, info).commit();
        ft.commit();


        android.support.v4.app.FragmentManager fm1=getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft1=fm1.beginTransaction();
        PitanjeFrag pitanje= new PitanjeFrag();
       // ft1.replace(R.id.fragment_container1,pitanje );
        fm1.beginTransaction().replace(R.id.fragment_container1, pitanje ).commit();
        final Bundle bundle1 = new Bundle();
        bundle1.putParcelableArrayList("kviz", primljeniKvizovi);
        bundle1.putString("odabraniKviz",odabraniKviz );
        pitanje.setArguments(bundle1);
        ft1.commit();

    }


    @Override
    public void inputInformacija(Kviz n) {

    }

    @Override
    public void inputPitanje(Pitanje p,int vel) {
            info.novaMetoda(p,vel);
    }
}
