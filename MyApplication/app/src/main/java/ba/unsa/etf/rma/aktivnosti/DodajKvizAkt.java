package ba.unsa.etf.rma.aktivnosti;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.klase.Adapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodajKvizAkt extends AppCompatActivity {
     ArrayList<String> nPitanja=new ArrayList<>();
     ArrayList<Pitanje> pPitanja=new ArrayList<>();
     ArrayList<String> pKategorije=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kviz_akt);

        final ListView lvDodanaPitanja=(ListView) findViewById(R.id.lvDodanaPitanja);
        final ListView lvMogucaPitanja=(ListView) findViewById(R.id.lvMogucaPitanja);
        final Spinner spKategorije=(Spinner) findViewById(R.id.spKategorije);
        final EditText etNaziv=(EditText) findViewById(R.id.etNaziv);
        final Button btnDodajKviz=(Button) findViewById(R.id.btnDodajKviz);
        final TextView mPitanja=(TextView) findViewById(R.id.mPitanja);
        final TextView pUKvizu=(TextView) findViewById(R.id.pUKvizu);
        final ArrayList<Pitanje> mogPitanja=new ArrayList<>();
        final Button btnImportKviz=(Button) findViewById(R.id.btnImportKviz);
        ArrayList<String> o=new ArrayList<>();
        o.add("RMA");
        o.add("AFJ");
        Pitanje novo=new Pitanje("Pitanje 1","Koji je ovo predmet",o,"RMA");
        mogPitanja.add(novo);
        ArrayList<String> o1=new ArrayList<>();
        o.add("Sarajevo");
        o.add("Mostar");
        Pitanje novo1=new Pitanje("Pitanje 2","Koji je glavni grad BiH",o,"Sarajevo");
        mogPitanja.add(novo1);

        Bundle intent= getIntent().getExtras();
        Intent in1=getIntent();
        final String odabraniKviz=in1.getStringExtra("odabrani");
        final ArrayList<Kviz> primljeniKvizovi= intent.getParcelableArrayList("kvizovi");
        Kviz trazeni=new Kviz();
        final ArrayList<Kategorija> primljenjeKategorije = intent.getParcelableArrayList("kat");
        etNaziv.setText(odabraniKviz);
        pKategorije=new ArrayList<>();
        //pKategorije.add("Dodaj kategoriju");                                                        //za spinner postojecih kategorija
        for(int i=0;i<primljenjeKategorije.size();i++){
            pKategorije.add(primljenjeKategorije.get(i).getNaziv());
        }
        pKategorije.add("Dodaj kategoriju");
        int pozicija=0;
        ArrayAdapter<String> ad=new ArrayAdapter<>(DodajKvizAkt.this,android.R.layout.simple_list_item_1,pKategorije);
        spKategorije.setAdapter(ad);
        Pitanje dodaj=new Pitanje("Dodaj pitanje","Dodaj pitanje",null," ");
        pPitanja.add(dodaj);
        for(int i =0;i<primljeniKvizovi.size();i++){                                                //za listview postojecih pitanja
            if(primljeniKvizovi.get(i).getNaziv().equals(odabraniKviz)){
                pPitanja=primljeniKvizovi.get(i).getPitanja();
                pozicija=i;
            }
        }

        if(pPitanja!=null){
        for(int i=0;i<pPitanja.size();i++){
            nPitanja.add(pPitanja.get(i).getNaziv());
        }}

        nPitanja.add(dodaj.getNaziv());
        final ArrayAdapter<String> ad1=new ArrayAdapter<>(DodajKvizAkt.this,android.R.layout.simple_list_item_1,nPitanja);
        lvDodanaPitanja.setAdapter(ad1);
        final ArrayList<String> mogucaPitanja=new ArrayList<>();                                          //za listview mogucih pitanja
        for(int i=0;i<mogPitanja.size();i++){
            mogucaPitanja.add(mogPitanja.get(i).getNaziv());
        }
        final ArrayAdapter<String> ad2=new ArrayAdapter<>(DodajKvizAkt.this,android.R.layout.simple_list_item_1,mogucaPitanja);
        lvMogucaPitanja.setAdapter(ad2);

        etNaziv.setText(odabraniKviz);
        spKategorije.setSelection(pozicija);

        spKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spKategorije.getItemAtPosition(position).toString().equals("Dodaj kategoriju")){
                    Intent novi=new Intent(DodajKvizAkt.this,DodajKategorijuAkt.class);
                    startActivityForResult(novi,12 );
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        lvDodanaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String odabrana= (String) lvDodanaPitanja.getItemAtPosition(position);
                if(odabrana.equals("Dodaj pitanje")){
                        Intent intent2=new Intent(DodajKvizAkt.this,DodajPitanjeAkt.class);
                        intent2.putParcelableArrayListExtra("xx", primljeniKvizovi);
                        startActivityForResult(intent2, 2);
                }
                else{
                    nPitanja.remove(odabrana);
                    mogucaPitanja.add(odabrana);
                    ad1.notifyDataSetChanged();
                    ad2.notifyDataSetChanged();
                    for(int i=0;i<primljeniKvizovi.size();i++){
                        if(primljeniKvizovi.get(i).getNaziv().equals(odabraniKviz)){
                           for(int j=0;j<primljeniKvizovi.get(i).getPitanja().size();j++) {
                               if (primljeniKvizovi.get(i).getPitanja().get(j).getNaziv().equals(odabrana)) {
                                   mogPitanja.add(primljeniKvizovi.get(i).getPitanja().get(j));
                                   primljeniKvizovi.remove(primljeniKvizovi.get(i).getPitanja().get(j));
                               }
                           }
                        }
                    }
                }
            }
        });
        lvMogucaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String odabrana = (String) lvMogucaPitanja.getItemAtPosition(position);
                nPitanja.add(odabrana);
                mogucaPitanja.remove(odabrana);
                ad1.notifyDataSetChanged();
                ad2.notifyDataSetChanged();
                for (int i = 0; i < mogPitanja.size(); i++) {
                    if (mogPitanja.get(i).getNaziv().equals(odabrana)) {
                        Pitanje n = mogPitanja.get(i);
                        for (int j = 0; j < primljeniKvizovi.size(); j++) {
                            if (primljeniKvizovi.get(j).getNaziv().equals(odabraniKviz)) {
                                primljeniKvizovi.get(j).getPitanja().add(n);
                                pPitanja.add(n);
                            }
                        }
                        mogPitanja.remove(mogPitanja.get(i));
                    }
                }
            }
        });
        final int finalPozicija = pozicija;
        btnDodajKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String kateg=(String) spKategorije.getItemAtPosition(finalPozicija);
                Kviz novi;
                Kategorija kateg1=new Kategorija(" "," ");
                for(int i=0;i<primljenjeKategorije.size();i++){
                    if(primljenjeKategorije.get(i).getNaziv().equals(kateg)){
                        kateg1=primljenjeKategorije.get(i);
                    }
                }
                novi= new Kviz(etNaziv.getText().toString(),pPitanja,kateg1);
                for(int i=0;i<primljeniKvizovi.size();i++){
                    if(primljeniKvizovi.get(i).getNaziv().equals(odabraniKviz)){
                        if(odabraniKviz=="Dodaj kviz"){
                            primljeniKvizovi.add(novi);
                        }else{
                            novi.setPitanja(pPitanja);
                        primljeniKvizovi.add(novi);
                        primljeniKvizovi.remove(primljeniKvizovi.get(i));}
                    }
                }
                Intent intent2=new Intent(DodajKvizAkt.this,KvizoviAkt.class);
                intent2.putParcelableArrayListExtra("noviKvizovi", primljeniKvizovi);
                setResult(1, intent2);
                finish();
            }
        });
        btnImportKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent().setType("text/plain").setAction(Intent.ACTION_OPEN_DOCUMENT);
                startActivityForResult(Intent.createChooser(intent, "Select a TXT file"), 123);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            final ListView lvDodanaPitanja=(ListView) findViewById(R.id.lvDodanaPitanja);
            Pitanje result= (Pitanje) data.getParcelableExtra("noviKvizovi");
            nPitanja.add(result.getNaziv());
            pPitanja.add(result);
            final ArrayAdapter<String> ad1=new ArrayAdapter<>(DodajKvizAkt.this,android.R.layout.simple_list_item_1,nPitanja);
            lvDodanaPitanja.setAdapter(ad1);
            ad1.notifyDataSetChanged();
        }
        if(requestCode==123 ) {
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
                InputStream inputStream = null;
                try {
                    inputStream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line= null;
                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                while (line  != null) {
                    stringBuilder.append(line);
                }
                String tekst=stringBuilder.toString();
                final EditText etNaziv=(EditText) findViewById(R.id.etNaziv);
                //fileInputStream.close();
                //parcelFileDescriptor.close();
                String nazivKviza=new String();
                int i=0;
                    while(tekst.charAt(i)!=','){
                        nazivKviza+=tekst.charAt(i);
                        i++;
                    }
                etNaziv.setText(nazivKviza);                                                        // naziv kviza
                if(nPitanja.contains(nazivKviza)){                                                  // allertDialog, greska
                    AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                    alertDialog.setTitle("Paznja");
                    alertDialog.setMessage("Kviz kojeg importujete već postoji!");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                String katKviza=new String();
                i++;                                                                                 //kategorija
                while(tekst.charAt(i)!=','){
                    katKviza+=tekst.charAt(i);
                    i++;
                }
                final Spinner spKategorije=(Spinner) findViewById(R.id.spKategorije);               //spinner kategorija
                if(pKategorije.contains(katKviza)){
                    ArrayAdapter<String> ad=new ArrayAdapter<>(DodajKvizAkt.this,android.R.layout.simple_list_item_1,pKategorije);
                    spKategorije.setAdapter(ad);
                    int pozicija=0;
                    for(int j=0;j<pKategorije.size();j++){
                        if(pKategorije.get(j).equals(katKviza)){
                            pozicija=j;
                        }
                    }
                    spKategorije.setSelection(pozicija);
                }
                else{
                    pKategorije.add(katKviza);
                    int pozicija=0;
                    for(int j=0;j<pKategorije.size();j++){
                        if(pKategorije.get(j).equals(katKviza)){
                            pozicija=j;
                        }
                    }
                    spKategorije.setSelection(pozicija);
                }                                                                                   //za kategorije u spinneru
                i++;
                String brojPitanja=new String();
                while(tekst.charAt(i)!='\n'){
                    brojPitanja+=tekst.charAt(i);
                }
                int navodnaVelicina=Integer.parseInt(brojPitanja);                                  // broj pitanja u kvizu
                int velicinaKviza=0;
                int k=0; int brojac=0;
                while(k!=tekst.length()){
                    if(tekst.charAt(k)=='\n'){
                        brojac++;
                    }
                    k++;
                }
                if(navodnaVelicina!=(velicinaKviza-1)){
                    AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                    alertDialog.setTitle("Paznja");
                    alertDialog.setMessage("Kviz kojeg imporujete ima neispravan broj pitanja!");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                ArrayList<String> pitanja=new ArrayList<>();
                String jednoPitanje=new String();
                i++;
                while (i!=tekst.length()) {
                    while (tekst.charAt(i) != ',') {
                        jednoPitanje += tekst.charAt(i);
                        i++;
                    }
                    pitanja.add(jednoPitanje);
                    while (tekst.charAt(i) != '\n') {
                        i++;
                    }
                    i++;
                }
                final ListView lvDodanaPitanja=(ListView) findViewById(R.id.lvDodanaPitanja);
                final ArrayAdapter<String> ad1=new ArrayAdapter<>(DodajKvizAkt.this,android.R.layout.simple_list_item_1,pitanja);
                lvDodanaPitanja.setAdapter(ad1);
            }
        }
    }
}
