package ba.unsa.etf.rma.ba.unsa.etf.rma.fragmenti;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.klase.Adapter;
import ba.unsa.etf.rma.klase.GridAdapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;

public class DetailFrag extends Fragment {


    private OnFragmentInteractionListener mListener;

    public DetailFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final GridView gridKvizovi=(GridView) getActivity().findViewById(R.id.gridKvizovi);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void novaMetoda(final ArrayList<Kviz> kvizovi, String odabrana, final ArrayList<Kategorija> sve){
        final GridView gridKvizovi=(GridView) getActivity().findViewById(R.id.gridKvizovi);
        final GridAdapter adapter1 = new GridAdapter(getActivity(), kvizovi);
        gridKvizovi.setAdapter(adapter1);
        gridKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent2 = new Intent(getActivity(), IgrajKvizAkt.class);
                TextView temp = (TextView) view.findViewById(R.id.nazivKviz);
                String nazivOdabranog = temp.getText().toString();
                intent2.putExtra("odabrani", nazivOdabranog.toString());
                //intent2.putParcelableArrayListExtra("kat", sveKategorije);
                intent2.putParcelableArrayListExtra("kvizovi", kvizovi);
                startActivityForResult(intent2, 99);
            }
        });
        gridKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                TextView temp = (TextView) view.findViewById(R.id.nazivKviz);
                String nazivOdabranog = temp.getText().toString();
                Intent intent1 = new Intent(getActivity(), DodajKvizAkt.class);
                intent1.putExtra("odabrani", nazivOdabranog.toString());
                intent1.putParcelableArrayListExtra("kat", sve);
                intent1.putParcelableArrayListExtra("kvizovi", kvizovi);
                startActivityForResult(intent1, 1);
                return false;
            }
        });
    }
}
