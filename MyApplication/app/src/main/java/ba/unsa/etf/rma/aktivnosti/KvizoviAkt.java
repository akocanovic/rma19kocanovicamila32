package ba.unsa.etf.rma.aktivnosti;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.klase.Adapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

import static android.widget.AdapterView.*;
import static java.lang.Integer.valueOf;

public class KvizoviAkt extends AppCompatActivity implements ListaFrag.OnFragmentInteractionListener,DetailFrag.OnFragmentInteractionListener {
    ArrayList<Kviz> sviKvizovi=new ArrayList<>();
    final ArrayList<Kategorija> sveKategorije=new ArrayList<>();
    ListaFrag lista=new ListaFrag();
    DetailFrag detail=new DetailFrag();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kvizovi_akt);

        final Spinner spPostojeceKategorije= (Spinner) findViewById(R.id.spPostojeceKategorije);
        final ListView lvKvizovi= (ListView) findViewById(R.id.lvKvizovi);
        ArrayList<String> o=new ArrayList<>();
        ArrayList<Pitanje> pitanjaKviz=new ArrayList<>();
        o.add("ETF");
        o.add("PMF");
        Pitanje novo=new Pitanje("Pitanje ","Koji je ovo fakultet",o,"ETF");
        pitanjaKviz.add(novo);
        //sveKategorije=new ArrayList<>();
        Kategorija kateg=new Kategorija("Svi","00");
        Kategorija kv1=new Kategorija("Nauka","01");
        Kategorija kv2=new Kategorija("Sport","02");
        ArrayList<String> o1=new ArrayList<>();
        ArrayList<Pitanje> pitanjaKviz1=new ArrayList<>();
        o1.add("Olimpik");
        o1.add("Sarajevo");
        Pitanje novo1=new Pitanje("Pitanje 3 ","Koji je najbolji klub u Sarajevu",o1,"Sarajevo");
        pitanjaKviz1.add(novo);
        pitanjaKviz1.add(novo1);
        final Kviz prazan=new Kviz();
        Kviz kviz1=new Kviz();
        kviz1.setNaziv("Kviz 1");
        kviz1.setKategorija(kv1);
        kviz1.setPitanja(pitanjaKviz);
        sviKvizovi.add(kviz1);
        Kviz kviz2=new Kviz();
        kviz2.setNaziv("Kviz 2");
        kviz2.setKategorija(kv2);
        kviz2.setPitanja(pitanjaKviz1);

        sviKvizovi.add(kviz2);

        sveKategorije.add(kateg);
        ArrayList<String> kategorije=new ArrayList<>();
        ArrayList<String> kvizovi=new ArrayList<>();
        for(int i=0;i<sviKvizovi.size();i++){
            Kategorija kat=sviKvizovi.get(i).getKategorija();
            String novo3=kat.getNaziv();
            String kvi;
            kvi=sviKvizovi.get(i).getNaziv();
            if(kategorije.contains(kat)==false){
                kategorije.add(novo3);
                sveKategorije.add(kat);
            }
            if(kvizovi.contains(kvi)==false){
                kvizovi.add(kvi);
            }
        }
        int screenLayout = getResources().getConfiguration().screenWidthDp;

        if(screenLayout<550) {
            kategorije.add("Svi");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, kategorije);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spPostojeceKategorije.setAdapter(adapter);
            spPostojeceKategorije.setSelection(0);
            final Adapter adapter1 = new Adapter(KvizoviAkt.this, sviKvizovi);
            lvKvizovi.setAdapter(adapter1);

            spPostojeceKategorije.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String odabrana = (String) spPostojeceKategorije.getItemAtPosition(position);
                    if (odabrana.equals("Svi")) {
                        final Adapter adapter2 = new Adapter(KvizoviAkt.this, sviKvizovi);
                        lvKvizovi.setAdapter(adapter2);
                        adapter2.notifyDataSetChanged();
                    } else {
                        ArrayList<Kviz> sviKvizovi1 = new ArrayList<Kviz>(sviKvizovi);
                        for (int i = 0; i < sviKvizovi1.size(); i++) {
                            if (sviKvizovi1.get(i).getKategorija().getNaziv().equals(odabrana) || sviKvizovi1.get(i).getNaziv().equals("Dodaj kviz")) {

                            } else {
                                sviKvizovi1.remove(sviKvizovi1.get(i));
                            }
                        }
                        final Adapter adapter3 = new Adapter(KvizoviAkt.this, sviKvizovi1);
                        lvKvizovi.setAdapter(adapter3);
                        adapter3.notifyDataSetChanged();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            lvKvizovi.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent2 = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                    TextView temp = (TextView) view.findViewById(R.id.naziv);
                    String nazivOdabranog = temp.getText().toString();
                    intent2.putExtra("odabrani", nazivOdabranog.toString());
                    intent2.putParcelableArrayListExtra("kat", sveKategorije);
                    intent2.putParcelableArrayListExtra("kvizovi", sviKvizovi);
                    startActivityForResult(intent2, 99);
                }
            });
            lvKvizovi.setOnItemLongClickListener(new OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView temp = (TextView) view.findViewById(R.id.naziv);
                    String nazivOdabranog = temp.getText().toString();
                    Intent intent1 = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                    intent1.putExtra("odabrani", nazivOdabranog.toString());
                    intent1.putParcelableArrayListExtra("kat", sveKategorije);
                    intent1.putParcelableArrayListExtra("kvizovi", sviKvizovi);
                    startActivityForResult(intent1, 1);
                    return false;
                }
            });
        }if(screenLayout>=550){
            android.support.v4.app.FragmentManager fragmentManager= getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction ft= fragmentManager.beginTransaction();
            final Bundle bundle = new Bundle();
            kategorije.add("Svi");
            bundle.putStringArrayList("kviz", kategorije);
            lista.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.listPlace, lista).commit();
            ft.commit();
            android.support.v4.app.FragmentManager fragmentManager1= getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction ft1= fragmentManager.beginTransaction();
            final Bundle bundle1 = new Bundle();
            bundle1.putParcelableArrayList("kviz", sveKategorije);
            detail.setArguments(bundle1);
            fragmentManager1.beginTransaction().replace(R.id.detailPlace, detail).commit();
            ft1.commit();}
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
                ArrayList<Kviz> result= data.getParcelableArrayListExtra("noviKvizovi");
                sviKvizovi=new ArrayList<Kviz>(result);
                final ListView lvKvizovi= (ListView) findViewById(R.id.lvKvizovi);
                final Adapter adapter10 = new Adapter(KvizoviAkt.this, result);
                lvKvizovi.setAdapter(adapter10);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentInteraction(String kategorija) {
        if(kategorija.equals("Svi")){
            detail.novaMetoda(sviKvizovi,kategorija,sveKategorije);
        }else{
        ArrayList<Kviz> odabraniKvizovi=new ArrayList<>();
            for(int i =0;i<sviKvizovi.size();i++){
                if(sviKvizovi.get(i).getKategorija().getNaziv().equals(kategorija)){
                    odabraniKvizovi.add(sviKvizovi.get(i));
                }
            }
        detail.novaMetoda(odabraniKvizovi,kategorija,sveKategorije);}
    }
}
