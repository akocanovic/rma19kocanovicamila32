package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.amila.myapplication.R;

public class GridAdapter extends ArrayAdapter<Kviz> {
    public GridAdapter(@NonNull Context context, ArrayList<Kviz> kvizovi) {
        super(context, R.layout.element_grida,kvizovi );
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Kviz kviz=getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_grida, parent, false);
        }
        final TextView nazivKviz= (TextView) convertView.findViewById(R.id.nazivKviz);
        final ImageView slikaKat= (ImageView) convertView.findViewById(R.id.slikaKat);
        final TextView brPitanja=(TextView) convertView.findViewById(R.id.brPitanja);

        nazivKviz.setText(kviz.naziv);
        if(kviz.getPitanja()==null){ brPitanja.setText("0");}else{
        brPitanja.setText(String.valueOf(kviz.getPitanja().size()));}
        slikaKat.setImageResource(R.drawable.ic_launcher_background);
        return convertView;
    }
}
