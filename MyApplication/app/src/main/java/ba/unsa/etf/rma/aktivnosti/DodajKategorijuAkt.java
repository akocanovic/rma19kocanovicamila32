package ba.unsa.etf.rma.aktivnosti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.klase.Kategorija;

public class DodajKategorijuAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kategoriju_akt);

        final EditText etNaziv=(EditText) findViewById(R.id.etNaziv);
        final EditText etIkona=(EditText) findViewById(R.id.etIkona);
        final Button btnDodajIkonu=(Button) findViewById(R.id.btnDodajIkonu);
        final Button btnDodajKategoriju=(Button) findViewById(R.id.btnDodajKategoriju);

        String novaKategorija=etNaziv.getText().toString();
        String noviId=etIkona.getText().toString();
        Kategorija novaKat=new Kategorija(novaKategorija,noviId);
        Intent intent2=new Intent(DodajKategorijuAkt.this,DodajKvizAkt.class);
        intent2.putExtra("nova kategorija", novaKat);
        startActivityForResult(intent2, 12);


    }
}
