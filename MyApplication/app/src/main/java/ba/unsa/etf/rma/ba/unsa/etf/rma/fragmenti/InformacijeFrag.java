package ba.unsa.etf.rma.ba.unsa.etf.rma.fragmenti;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;

import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.amila.myapplication.R;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;


public class InformacijeFrag extends Fragment {

    public interface  fragmentInformacije{
        void inputInformacija(Kviz n);
    }

    public InformacijeFrag() {
        // Required empty public constructor
    }
    public static InformacijeFrag newInstance(String param1, String param2) {
        InformacijeFrag fragment = new InformacijeFrag();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_informacije, container, false);
    }
    Kviz nasKviz=new Kviz();
    int pocetnostanje=1;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final TextView infNazivKviza=(TextView) getView().findViewById(R.id.infNazivKviza);
        final TextView nazivKviza= (TextView) getView().findViewById(R.id.nazivKviza);
        final TextView infBrojTacnihPitanja=(TextView) getView().findViewById(R.id.infBrojTacnihPitanja);
        final TextView brojTacnih=(TextView) getView().findViewById(R.id.brojTacnih);
        final TextView infBrojPreostalihPitanja=(TextView) getView().findViewById(R.id.infBrojPreostalihPitanja);
        final TextView brojPreostalih=(TextView) getView().findViewById(R.id.brojPreostalih);
        final TextView infProcenatTacni= (TextView) getView().findViewById(R.id.infProcenatTacni);
        final TextView procenatTacnih=(TextView) getView().findViewById(R.id.procenatTacnih);
        final Button btnKraj=(Button) getView().findViewById(R.id.btnKraj);

        final String kviz= getArguments().getString("odabraniKviz");
        final ArrayList<Kviz> primljeniKvizovi= getArguments().getParcelableArrayList("kviz");

        assert kviz != null;
        nazivKviza.setText(kviz);
        for(int i=0;i<primljeniKvizovi.size();i++){
            if(primljeniKvizovi.get(i).getNaziv()==kviz){
                nasKviz=primljeniKvizovi.get(i);
            }
        }
        pocetnostanje=nasKviz.getPitanja().size();
        int brPitanja=nasKviz.getPitanja().size();
        brojPreostalih.setText(String.valueOf(brPitanja));
        brojTacnih.setText("0");
        procenatTacnih.setText("0");

        btnKraj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), KvizoviAkt.class);
                startActivity(intent);
            }

        });
    }
    double brojTacnihOdg=0;
    double procenat=100.00;
    double brojac=0;


    public void novaMetoda(Pitanje novo,int velicina){
        brojac++;
        final TextView brojTacnih=(TextView) getView().findViewById(R.id.brojTacnih);
        final TextView brojPreostalih=(TextView) getView().findViewById(R.id.brojPreostalih);
        final TextView procenatTacnih=(TextView) getView().findViewById(R.id.procenatTacnih);
        if(velicina!=0){
        brojPreostalih.setText(String.valueOf(velicina-1));}
        String tacan=novo.getTacan();
        pocetnostanje=1;
        if(tacan.equals("xx")){
            brojTacnihOdg++;
            if(brojTacnihOdg!=0){
             procenat=(double)(brojTacnihOdg/brojac)*100;}
        }else{
            if(brojTacnihOdg!=0){
            procenat=(double)(brojTacnihOdg/brojac)*100;
            }
        }
        brojTacnih.setText( String.valueOf(brojTacnihOdg));
        procenatTacnih.setText( String.valueOf(procenat)+ " %");

    }


}
